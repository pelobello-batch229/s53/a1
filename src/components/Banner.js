// Destructuring components/modules from cleaner codebase
import {Button, Row, Col} from 'react-bootstrap';
import PageNotFound from '../pages/PageNotFound.js'
export default function Banner() {
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere!</p>
				<Button variants="primary">Enroll Now!</Button>
			</Col>
		</Row>

		)
}